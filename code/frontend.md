# 6/21/2018

For creating in border text titles


```html
<div class="box">
    <h3>Title</h3>
</div>



<style>
.box {
    border: 1px solid black;
    margin-top:1rem;
    
        
}
.box > h3 {
    margin-top: -10px;
    margin-left: 5px;
    background: white;
    font-size: 14px;
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
}

</style>
```